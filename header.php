<?php
//tidak dapat membuka page ini jika tidak login
session_start();
if (!isset($_SESSION['nama'])) {
	header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Peduli Diri - <?= $_SESSION['nama']; ?></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="kepala">
		<div class="head">
			<img src="suki-bilek.png">
			<div>
				<h1>Peduli Diri</h1>
				<p>Catatan Perjalanan</p>
			</div>
		</div>
		<div class="nav">
			<a href="index.php">Home</a>
			<a href="catatan.php">Catatan Perjalanan</a>
			<a href="input.php">Isi Data</a>
			<a style="color: red;" href="logout.php">Logout</a>
		</div>
	</div>