<?php

session_start();
//menghancurkan session akan kembali ke keadaan kosong
session_destroy();

//akan diarahkan ke page login lagi
header("Location: login.php");

?>