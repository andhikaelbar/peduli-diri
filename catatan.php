<?php 
//menyambungkan dengan file header.php
include 'header.php';

//menyambungkan dengan file nik yg telah diinput .txt
$array = file_get_contents($_SESSION['nik'].".txt");
$catatan = json_decode($array,true);
?>
	<div class="urut">
		<p>Urutkan Berdasarkan
			<select>
				<option>Tanggal</option>
				<option>Waktu</option>					
				<option>Lokasi</option>
				<option>Suhu</option>
			</select>
			<button>urutkan</button>
		</p>
	</div>
	<div class="content" style="margin-top: 10px;">
		<center>
			<table border="1" style="width: 800px;">
				<tr>
					<th>Tanggal</th>
					<th>Waktu</th>
					<th>Lokasi</th>
					<th>Suhu Tubuh</th>
				</tr>
				<?php
				//untuk mengambil data dari Array
				foreach ($catatan as $key => $value) { ?>
				<tr>
					<td><?= $value['tanggal']; ?></td>
					<td><?= $value['jam']; ?></td>
					<td><?= $value['lokasi']; ?></td>
					<td><?= $value['suhu']; ?> °C</td>
				</tr>
				<?php } ?>
			</table>
		</center>
	</div>

<?php 
//menyambungkan dengan file footer.php
include 'footer.php'; 
?>