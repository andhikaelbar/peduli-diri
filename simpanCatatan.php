<?php

$data = [
	'tanggal' => $_POST['tanggal'],
	'jam' => $_POST['jam'],
	'lokasi' => $_POST['lokasi'],
	'suhu' => $_POST['suhu']
];

//supaya tau siapa kita dari data login
session_start();

//ambil data dari file nik yg telah diinput .txt
$array = file_get_contents($_SESSION['nik'].".txt");
$catatan = json_decode($array, true);

array_push($catatan, $data);

//simpan data di dalam file nik yg telah diinput .txt
$catatan_data = json_encode($catatan);
file_put_contents($_SESSION['nik'].".txt", $catatan_data);

//setelah selesai input data catatan akan diarahkan ke page catatan
header("Location: catatan.php");

?>