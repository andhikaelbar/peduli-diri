<?php

$data = [
	'nik' => $_POST['nik'],
	'nama' => $_POST['nama']
];

$array = file_get_contents("user.txt");
$user = json_decode($array,true);

$login = false;
foreach ($user as $key => $value) {
	//jika data nik dan nama yg diinput sesuai yg ada di dalam Array akan berhasil melakukan login 
	if ($data['nama'] == $value['nama'] && $data['nik'] == $value['nik']) {
		$login = true;
		break;
	}
}

//jika login berhasil akan diarahkan ke halaman awal
if ($login) {
	session_start();
	$_SESSION['nik'] = $data['nik'];
	$_SESSION['nama'] = $data['nama'];
	header("Location: index.php");
} else {
	//tapi jika salah akan kembali ke halaman login
	header("Location: login.php");
}

?>