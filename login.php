<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div>
		<div class="form-login-register">
			<form method="POST" action="loginProses.php">
				<h2><center>Login</center></h2>
					<input class="row" placeholder="Nik" type="number" name="nik">
					<input class="row" placeholder="Nama Lengkap" type="text" name="nama">
				<div class="row-btn">
					<center>
						<input type="submit" value="Login">
						<p>Belum punya akun? <a href="register.php">Buat disini!</a></p>
					</center>
				</div>
			</form>
		</div>
	</div>
</body>
</html>